import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import './styles.css';

export default class DynamicCommentScroll extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //the comments we currently render
            commentsToRender: [],
            //the index for the current comment in the input-comment-list, first element will
            //be the 0th not the -5
            currentIndex: 0
        };
    }
///////////////////////////////////////////////////////////////////////////////////////////////
    //This method returns a new/the next comment
    getComment(index) {
        return this.props.comments[index];
    };
///////////////////////////////////////////////////////////////////////////////////////////////
    //This method decides if we can add 3 or less comments
    numCommentsToAdd(currentCommentsLen){
        var commentsToAdd = 3;

        if (currentCommentsLen + commentsToAdd > this.props.comments.length) {
            commentsToAdd = this.props.comments.length - currentCommentsLen;
        }
        return commentsToAdd;
    }
///////////////////////////////////////////////////////////////////////////////////////////////
    //This method updates the amount of comments to be shown when the load-button
    // was clicked
    clicked(){

        // a temporary list
        var currentComments = this.state.commentsToRender;

        //the amount of comments we are able to add
        const commentsToAdd = this.numCommentsToAdd(currentComments.length);

        //Adds the next 3 or less (depends on "commentsToAdd") new comments to the temporary
        // list (currentComments), activates if we've pressed the load more comments button
        for (var i = 0; i < commentsToAdd; i++) {
            currentComments.push(this.getComment(this.state.currentIndex+i));
        }
        //updating states
        this.setState({
            commentsToRender: currentComments,
            currentIndex: this.state.currentIndex + commentsToAdd
        })
    }

///////////////////////////////////////////////////////////////////////////////////////////////
    //This method returns one of two strings shown on the load-button
    //in case we can load more comments the button will say so, or if we can't
    //the button will say so too
    checkCommentList(){
        return(
        this.state.commentsToRender.length<this.props.comments.length?"Load more comments ...":"All comments loaded!"

        )
    }
    render() {
        var loadbutton = <Button className="loadingButton" onClick={this.clicked.bind(this)}>{this.checkCommentList()}</Button>;
        return (
            <div className="loadButton">
            {loadbutton}
                <div className="scrollableComments">
                    {this.state.commentsToRender}
                    <br/>
                </div>
            </div>
        );
    }
}