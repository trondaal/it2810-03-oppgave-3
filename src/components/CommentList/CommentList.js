import React, {Component} from "react";
import Comment from ".././Comment/Comment";
import CommentStore from "../../stores/CommentStore";
import SessionStore from "../../stores/SessionStore";
import DynamicCommentScroll from "./../DynamicCommentScroll/DynamicCommentScroll";

/*
This component will display a list of comments
*/

export default class CommentList extends Component {
  constructor(props) {
    super(props);
    this.getComments = this.getComments.bind(this);
    this.state = {
      comments : CommentStore.getComments()
    }
  }

  componentWillMount() {
    CommentStore.on("change", this.getComments);
    SessionStore.on("change", this.getComments);
  }

  componentWillUnmount() {
    CommentStore.removeListener("change", this.getComments)
    SessionStore.removeListener("change", this.getComments)
  }

  getComments() {
    console.log("Getting comments for", SessionStore.getOrgnr());
    console.log("commentstore lenght", CommentStore.getComments().length);
    this.setState({
      comments: CommentStore.getComments()
    })
  }



  render() {
    var outputcomments = [];
    for (var i = 0; i < this.state.comments.length; i++) {
      outputcomments.push(<Comment key={i} comment={this.state.comments[i]}/>)
    }
    outputcomments.reverse();

    return (
      <div>
          <DynamicCommentScroll comments={outputcomments} />
      </div>
    )

  }

}
