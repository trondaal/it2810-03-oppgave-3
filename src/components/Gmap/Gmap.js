import React, {Component} from "react"
import GoogleMap from 'google-map-react';
import Marker from './Marker';
import geocoder from 'google-geocoder';
import ResturantStore from '../../stores/ResturantStore';
import * as SessionActions from '../../actions/SessionActions';
import { browserHistory } from 'react-router';

export default class Gmap extends Component{
  constructor(props) {
    super(props);
    this.state = {markers: []}
  }

  componentDidMount(){
    this.getMarkers();
    ResturantStore.on("done",() => {this.getMarkers()});

  }

  getMarkers(){
    var markers = [];
    var geo = geocoder({
      key: "AIzaSyCeGpFtc_fp3ND0KCqxdXzVf9Q0XMJ0EKI"
    });
    var adresse = "";
    ResturantStore.getAllResturants().forEach( r => {
      adresse = r.adresse+", "+r.poststed;
      geo.find(adresse, (err,res) => {
        if(err) console.log(err);
        markers.push(

        <Marker lat={res[0].location.lat} lng={res[0].location.lng}
          onClick={() => {
            SessionActions.changeOrgnr(r.id);
            browserHistory.push('/detail')
          }}
        />)})
    });
    this.setState({markers:markers});
  }



  render() {
    return(
      <div style={
          { height:"500px",
            marginBottom: "70px"}}>
      <GoogleMap
        bootstrapURLKeys={{
          key: "AIzaSyCeGpFtc_fp3ND0KCqxdXzVf9Q0XMJ0EKI",
          lang: 'en'
        }}
        options={{scrollwheel: false}}
        defaultCenter={{ lat: 63.419, lng: 10.41}}
        defaultZoom={12}
      >
      {this.state.markers}
      </GoogleMap>
      </div>
    )
  }
}
