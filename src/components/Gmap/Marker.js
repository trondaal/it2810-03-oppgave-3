import React, {Component} from 'react';

import {markerStyle} from './markerStyles.js';

export default class Marker extends Component {
  render() {
    return (
       <div style={markerStyle} onClick={this.props.onClick}>
        <i className="fa fa-cutlery" aria-hidden="true"></i>
       </div>
    );
  }
}
