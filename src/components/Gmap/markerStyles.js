

const markerStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  fontSize: 18,
  fontWeight: "bold",
  backgroundColor: "#F57C00",
  color: "white",
  width: "30px",
  borderRadius: "30px",
  textAlign: "center",
  padding: "2px"
};

export {markerStyle};
