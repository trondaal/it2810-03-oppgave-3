import React, {Component} from "react";
import "./DetailView.css";

import CleanIcon from ".././ResturantView/CleanIcon";
import CommentList from ".././CommentList/CommentList";
import CommentForm from ".././CommentForm/CommentForm";
import StarRatingComponent from "react-star-rating-component";
import SessionStore from "../../stores/SessionStore";

import CommentStore from '../../stores/CommentStore';
import { browserHistory } from 'react-router';



export default class DetailView extends Component{
  constructor(){
    super();
    this.fetchData = this.fetchData.bind(this)
  }
  componentDidMount() {
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  fetchData() {
    if (this._mounted) {
      fetch("http://hotell.difi.no/api/json/mattilsynet/smilefjes/tilsyn?=&orgnummer="+SessionStore.getOrgnr())
        .then((response) => {
          response.json().then((data) => {
            data = data.entries[data.entries.length-1];  //get newest? rating
            this.setState({
              navn: data.navn,
              postnr: data.postnr,
              addr: data.adrlinje1,
              poststed: data.poststed,
              renhet: data.total_karakter
            })
          })
        });
        fetch("http://it2810-03.idi.ntnu.no:8080/api/comments/average/"+SessionStore.getOrgnr())
          .then((result) => {
            result.json().then((data) => {
              this.setState({
                average: data.average
              })
            })
          })
    }
  }

  componentWillMount() {
    this.setState({orgnr: SessionStore.getOrgnr()});
    this.fetchData();
    CommentStore.on("change", this.fetchData);
    console.log(SessionStore.getOrgnr());
    if (SessionStore.getOrgnr() === 0) {
      browserHistory.push('/')
    }
  }


  render() {
    const imageurl = "https://maps.googleapis.com/maps/api/streetview?location=\""+this.state.addr +" "+  this.state.postnr +" "+ this.state.poststed +"\"&size=600x400&key=AIzaSyAv7JMXSBUmFsszUDjGGEmMWm6QkLW7E3k";
    return (
      <div>
        <div className="detailinfo">
          <img src={imageurl} alt="The restaurant"/>
          <div className="detailaddress">
            <h3>{this.state.navn}</h3>
            <h4>{this.state.addr}</h4>
            <h4>{this.state.postnr + " " + this.state.poststed}</h4>
            <CleanIcon cleanStatus={this.state.renhet} />
              <StarRatingComponent name="displayrating" value={this.state.average} emptyStarColor="#BBB" editing={false} />
              <p className="avgText">{(this.state.average === 0) ? "Ingen anmeldelser" : this.state.average + " stjerner i gjennomsnitt"}</p>
          </div>
        </div>
        <CommentForm />
        <CommentList />
      </div>
    );
  }

}
