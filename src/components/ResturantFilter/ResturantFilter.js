/*
A component for filtering resturants, both based on the resturants smile supervision and the rating from our own users.
*/

import React, { Component } from 'react';
import StarRatingComponent from "react-star-rating-component";

import ResturantStore from '../../stores/ResturantStore';
import * as ResturantActions from "../../actions/ResturantActions";
import './style.css';

import openTab from ".././ResturantView/images/openTab.png";
import closeTab from ".././ResturantView/images/closeTab.png";
import Good from ".././ResturantView/images/good.png";
import Ok from ".././ResturantView/images/ok.png";
import Bad from ".././ResturantView/images/bad.png";

let smileList = null;
let ratingList = null;
let smileImage = <img src={openTab} alt="open drop-down menu" className="OpenImage"/>;
let ratingImage = <img src={openTab} alt="open drop-down menu" className="OpenImage"/>;

export default class ResturantFilter extends Component {
  constructor(props) {
    super(props);
    this.handleSmileChange = this.handleSmileChange.bind(this);
    this.handleRatingChange = this.handleRatingChange.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      showSmile: true, // The status for showing or hiding the smile filters.
      showRating: true, // The status for showing or hiding the rating filters.
      checked: ResturantStore.getAllFilters() // Gets the status for all filters.
    }
  }

  // Event handler which set filter status based on which checkboxes are checked.
  handleChange(event) {
    const value = event.target.value;
    ResturantActions.setFilter({
      value
    });
  }

  // If the show smile filter is clicked the page renders again where the filter for smile supervision is shown.
  handleSmileChange() {
    smileList = <ul className="FilterList">
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="good"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <img src={Good} alt="Good" className="SmileImageFilter"/>
          </label>
        </div>
      </li>
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="ok"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <img src={Ok} alt="Ok" className="SmileImageFilter"/>
          </label>
        </div>
      </li>
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="bad"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <img src={Bad} alt="Bad" className="SmileImageFilter"/>
          </label>
        </div>
      </li>
    </ul>;

    // Changes the show/hide image and the status for showing the filter.
    if (this.state.showSmile) {
      this.setState({
        showSmile: false
      });
      smileImage = <img src={closeTab} alt="close drop-down menu" className="OpenImage"/>;
      return [smileList, smileImage];
    } else {
      smileList = null;
      this.setState({
        showSmile: true
      });
      smileImage = <img src={openTab} alt="open drop-down menu" className="OpenImage"/>;
      return smileImage;
    }
  }

  // If the show rating filter is clicked the page renders again where the filter for each rating is shown.
  handleRatingChange() {
    ratingList = <ul className="FilterList">
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="one"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <div className="FilterRating">
              <StarRatingComponent name="commentRating" value={1} emptyStarColor="#BBB" />
            </div>
          </label>
        </div>
      </li>
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="two"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <div className="FilterRating">
              <StarRatingComponent name="commentRating" value={2} emptyStarColor="#BBB" />
            </div>
          </label>
        </div>
      </li>
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="three"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <div className="FilterRating">
              <StarRatingComponent name="commentRating" value={3} emptyStarColor="#BBB" />
            </div>
          </label>
        </div>
      </li>
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="four"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <div className="FilterRating">
              <StarRatingComponent name="commentRating" value={4} emptyStarColor="#BBB" />
            </div>
          </label>
        </div>
      </li>
      <li>
        <div className="checkbox">
          <label>
            <input
              type="checkbox"
              value="five"
              onChange={this.handleChange}
              className="regular-checkbox"
            />
            <div className="FilterRating">
              <StarRatingComponent name="commentRating" value={5} emptyStarColor="#BBB" />
            </div>
          </label>
        </div>
      </li>
    </ul>;

    // Changes the show/hide image and the status for showing the filter.
    if (this.state.showRating) {
      this.setState({
        showRating: false
      });
      ratingImage = <img src={closeTab} alt="close drop-down menu" className="OpenImage"/>;
      return [ratingList, ratingImage];
    } else {
      ratingList = null;
      this.setState({
        showRating: true
      });
      ratingImage = <img src={openTab} alt="open drop-down menu" className="OpenImage"/>;
      return ratingImage;
    }
  }

  // Renders the content with just the button with text and image first and depending on the value of showSmile/showRating renders new content.
  render() {
    return (
      <div className="FilterContainer">
        <div className="FilterItem">
          <button onClick={this.handleSmileChange} className="FilterButton">
          <h3>
            Smilefjes filter
            {smileImage}
            </h3>
          </button>
          {smileList}
          </div>

          <div className="FilterItem">
            <button onClick={this.handleRatingChange} className="FilterButton">
            <h3>
              Vurdering filter
              {ratingImage}
            </h3>
          </button>
          {ratingList}
        </div>
      </div>
    )
  }
}
