/*
Render different smile icon based on the rating from the supervision
*/
import React, {Component} from 'react';
import Good from "./images/good.png";
import Ok from "./images/ok.png";
import Bad from "./images/bad.png";

export default class CleanIcon extends Component {

  render() {
    let image;
    if (this.props.cleanStatus === "0" || this.props.cleanStatus === "1") {
      image = <img src={Good} alt="Good" className="SmileImage"/>
    } else if (this.props.cleanStatus === "2") {
        image = <img src={Ok}  alt="Ok" className="SmileImage"/>
    } else {
      image = <img src={Bad} alt="Bad" className="SmileImage"/>
    }

    return(
      <div>
        {image}
      </div>
    )
  }
}
