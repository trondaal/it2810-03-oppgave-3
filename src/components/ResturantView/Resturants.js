/*
 This component just renders the resturant view component.
*/

import React, { Component } from 'react';
import './style.css';
import ResturantView from './ResturantView';

export default class Resturants extends Component {

  render() {
    return (
      <div>
        <ResturantView />
      </div>
    )
  }
}
