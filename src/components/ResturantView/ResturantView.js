/*
Create the table including the resturants. Also possible to search, sort and filter the resturants.
*/
import React, { Component } from 'react';
import CleanIcon from './CleanIcon';
import ResturantFilter from '.././ResturantFilter/ResturantFilter';
import ResturantStore from '../../stores/ResturantStore';
import * as SessionActions from "../../actions/SessionActions"
import { browserHistory } from 'react-router'
import StarRatingComponent from "react-star-rating-component";

import closeSorting from "./images/closeSorting.png";
import openSorting from "./images/openSorting.png";

let toggleNameImage = <img src={openSorting} alt="openSortingName" className="openSorting"/>;
let toggleRatingImage = <img src={openSorting} alt="openSortingRating" className="openSorting"/>;

export default class ResturantView extends Component {
  constructor(props) {
    super(props);
    this.filterRestaurants = this.filterRestaurants.bind(this);
    this.sortResturantsAlphabetically = this.sortResturantsAlphabetically.bind(this);
    this.sortResturantsRating = this.sortResturantsRating.bind(this);
    this.state = {
      resName: "", // Create the resturant name, used for searching.
      resturants: ResturantStore.getAllResturants(), // Gets all the resturants from the store.
      toggleNameButton: true, // Status for sorting on name alphabetically.
      toggleRatingButton: true // Status for sorting on rating.
    }

  }

  // Method for handeling serach, and return matches.
  handleChange(event){
    this.setState({
			resName: event.target.value
		})
  }

  // Method for checking which filter is clicked, and then show the related resturants.
  filterRestaurants() {
    // If no filters is checked show all resturants.
    const filters = ResturantStore.getAllFilters();
    const shouldDisplayAll = !Object.keys(filters).some(f => filters[f] === true);

    if (shouldDisplayAll) {
      this.setState({
        resturants: ResturantStore.getAllResturants()
      });
      return;
    }

    // Iterates all the resturants and adds the the ressturant to a temp list if the resturant maches the filter.
    const allResturants = ResturantStore.getAllResturants();
    let tempResturants = [];
    for(var i = 0; i < allResturants.length-1; i++) {
      if (filters['good']) {
        if(allResturants[i].cleanRate === '0' || allResturants[i].cleanRate === '1') {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['ok']) {
        if(allResturants[i].cleanRate === '2') {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['bad']) {
        if(allResturants[i].cleanRate === '3') {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['one']) {
        if(Math.floor(allResturants[i].average) === 1) {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['two']) {
        if(Math.floor(allResturants[i].average) === 2) {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['three']) {
        if(Math.floor(allResturants[i].average) === 3) {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['four']) {
        if(Math.floor(allResturants[i].average) === 4) {
          tempResturants.push(allResturants[i])
        }
      }
      if (filters['five']) {
        if(Math.floor(allResturants[i].average) === 5) {
          tempResturants.push(allResturants[i])
        }
      }
    }
    this.setState({
      resturants: tempResturants
    });
  }

  // Sort the resturants alphabetically, first descending then ascending when the button is clicked again.
  sortResturantsAlphabetically() {
    let byName = ResturantStore.getAllResturants();
    if (this.state.toggleNameButton) {
      this.setState({
        toggleNameButton: false
      });
      toggleNameImage = <img src={closeSorting} alt="closeSorting" className="closeSorting"/>;
      byName.sort(function(a,b) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
      });
    } else {
      this.setState({
        toggleNameButton: true
      });
      toggleNameImage = <img src={openSorting} alt="openSorting" className="openSorting"/>;
      byName.sort(function(a,b) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        return x > y ? -1 : x < y ? 1 : 0;
      });
    }

    this.setState({
      resturants: byName
    });
}

// Sort the resturants on the rating, first descending then ascending when the button is clicked again.
sortResturantsRating() {
  let byRating = ResturantStore.getAllResturants();
  if(this.state.toggleRatingButton) {
    this.setState({
      toggleRatingButton: false
    });
    toggleRatingImage = <img src={closeSorting} alt="closeSortingRating" className="closeSorting"/>;
    byRating.sort(function(a,b) {
      let x = a.average;
      let y = b.average;
      return x > y ? -1 : x < y ? 1 : 0;
    });
  } else {
    this.setState({
      toggleRatingButton: true
    });
    toggleRatingImage = <img src={openSorting} alt="openSortingRating" className="openSorting"/>;
    byRating.sort(function(a,b) {
      let x = a.average;
      let y = b.average;
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }

  this.setState({
    resturants: byRating
  })

}

  // If a filter is clicked the ResturantFilter component sends a changes which then calls the filterRestuants function in this compnent.
  componentWillMount() {
    ResturantStore.on("change", this.filterRestaurants);
  }


  // Method for getting the detailed view for each resturant, where you can see all ratings and comments together with a image of the resturant collected from google.
  handleClick(orgnr) {
    console.log(orgnr, "clicked");
    SessionActions.changeOrgnr(orgnr);
    browserHistory.push('/detail')
  }


  render() {
    // Collects all resturants from the store.
    const allResturants = this.state.resturants;

    // Filters the resturants which matches the characters put into the search field, when searching for a resturant.
    let fileredItem = allResturants.filter(
			(resturant) => {return resturant.name.toLowerCase().indexOf(this.state.resName.toLowerCase()) !== -1;}
		);

    let _this = this;

    // The render returns a list with all the resturants and renders again depending on if the user are searching, filtering or sorting the table.
    return (
      <div className="ResturantContent">

        <h1>Spisesteder i Trondheim</h1>
        <div className="form-group">
          <input className="form-control" id="searchInput" type="text" value={this.state.resName} onChange={this.handleChange.bind(this)} placeholder="søk etter resturant.." />
        </div>
        <ResturantFilter />
        <div>
		 <table>
           <tbody className="resturantTable">
             <tr className="ResturantRow">
               <button onClick={this.sortResturantsAlphabetically} className="SortButton">
                 <th className="ResturantHeader">Navn {toggleNameImage}</th>
               </button>
               <th className="ResturantHeader">Smilefjes</th>
               <button onClick={this.sortResturantsRating} className="SortButton">
                 <th className="ResturantHeader">Vurdering {toggleRatingImage}</th>
               </button>
             </tr>
							  {fileredItem.map((resturant, i) =>  // Show each resturant in the table, together with clean status and our rating for food and the result from a search/sorting/filtering.
                  <tr className="ResturantRow clickable" key={resturant.id + i} onClick={() => {_this.handleClick(resturant.id)}}>
                    <td className="ResturantName">{resturant.name}</td>
                    <td className="ResturantSmile"><CleanIcon cleanStatus={resturant.cleanRate} /></td>
                    <td className="ResturantRating"><StarRatingComponent name="displayrating" value={resturant.average} emptyStarColor="#BBB" editing={false} /></td>
                  </tr>
							  )}
              </tbody>
							</table>
					</div>
      </div>
    )
  }

}
