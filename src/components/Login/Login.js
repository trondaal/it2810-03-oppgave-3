import React from "react";
import { FacebookLogin } from "react-facebook-login-component";
import './style.css';
import SessionStore from "../../stores/SessionStore";
import { Link } from "react-router";

class Login extends React.Component{

	constructor() {
		super();
		this.state = {login: false};
	}
	/*gets the response from facebook, whether the user was logged in,
	username, email and id */
	responseFacebook = (response) => {
		console.log(response);
		if(response.accessToken != null){						//checks if the login was succesful, if no access token is generated the login failed.
			SessionStore.changeUser(response.name,response.id);	//stores the username and id
			SessionStore.login();								//changes the login status
			this.setState({login: true});						//forces a render update
			var fbUrl = "http://graph.facebook.com/userid/picture"; //standard url for profile pictures for the Graph API
			var rep = fbUrl.replace("userid", SessionStore.getFbid()); //replaces the userid with the users actual id
			SessionStore.setFbUrl(rep);							//stores the replacd url for later use
			document.getElementById("image").src = rep;			//replaces the .src with the profile picture from facebook
			}
	};


	render() {

		if( this.state.login === false) {
			return (
				<FacebookLogin
				socialId="1756965621223028"     //app ID
            	language="no"					//sets language
	            scope="public_profile"			//defines what information is gathered about the user
	            fields="name,email,picture"				//selects the fields we are interested in
	            responseHandler={this.responseFacebook}
	            version="v2.8"					//sets version
	            autoload={true}
	            className="facebook-login"
	          	buttonText="Logg inn med Facebook"
				data-size="large"
				id="LoginButton"/>);
		}
		else {

			return (
				<div className="fbUser">
				<Link to="/profile">{SessionStore.getNavn()}</Link>
				<img id="image" src="" alt="Facebook profile image can't be loaded." width="75" height="60"></img>
				</div>);


		}
	}
}

export default Login;
