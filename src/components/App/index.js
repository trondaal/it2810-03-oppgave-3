import React, { Component } from 'react';
import './style.css';
import Navbar from "../Navbar/Navbar";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <div className="Content">

          {this.props.children}
        </div>
      </div>
    );
  }
}
