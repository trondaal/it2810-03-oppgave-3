import React from "react";
import { nav } from 'react-bootstrap';
import { Link } from "react-router";
import FSLogo from "./logo.png";
import './style.css';
import Login from "../Login/Login";


/*
 This component will display a navbar with the web page's logo in the top left corner
 and the login button in the top right corner
 */

export default class Navbar extends React.Component{

    render(){
     return(
         <nav className="navbar navbar-inverse">
          <div className="ContentNavbar">

            <Link to="/"><img src={FSLogo} alt="Finspist logo" className="logo"/></Link>
            <Login />
            
          </div>
         </nav>

        );
    }
}
