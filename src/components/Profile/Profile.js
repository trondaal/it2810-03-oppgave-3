import React from "react";
import SessionStore from "../../stores/SessionStore";
import "./profile.css";
import * as SessionActions from "../../actions/SessionActions"
import CommentList from ".././CommentList/CommentList"
import { browserHistory } from 'react-router'


class Profile extends React.Component{
	constructor() {
		super();
		this.state = {total: 0, average: 0}
	}

  componentWillMount() {

		if (SessionStore.getFbid() == null) {
			browserHistory.push('/')
		}

		SessionActions.changeOrgnr(-1);
		fetch("http://it2810-03.idi.ntnu.no:8080/api/user/comments/average/"+SessionStore.getFbid())
		.then((result) => {
			result.json().then((data) => {
			  this.setState({
				average: data.average
			  })
			})
		});
		fetch("http://it2810-03.idi.ntnu.no:8080/api/user/comments/total/"+SessionStore.getFbid())
		.then((result) => {
			result.json().then((data) => {
			  this.setState({
				total: data.total
			  })
			})
		})
    }


	render() {
		console.log(this.state);
		return(
			<div>
				<div id="proName">
					<h1>{SessionStore.getNavn()}</h1>
				</div>
				<div id="stats">
				<h2>Stats!</h2>
					<ul>
						<li>Antall Kommentarer: {this.state.total}</li>
						<li>Gjennomsnittlig Vurdering: {this.state.average}</li>
					</ul>
					<h2>Dine Kommentarer</h2>
					<CommentList />
				</div>
				<div id="comments">
				</div>
			</div>
			);

	}
}

export default Profile;
