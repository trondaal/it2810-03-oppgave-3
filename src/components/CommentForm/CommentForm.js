import React, {Component} from "react";
import "./CommentForm.css";

import { FormGroup,FormControl,Button,HelpBlock } from 'react-bootstrap';
import StarRatingComponent from "react-star-rating-component";
import * as CommentActions from "../../actions/CommentActions";
import SessionStore from "../../stores/SessionStore";
import "../../stores/CommentStore";
/*
This component will add a comment to the current restaurant
*/

export default class CommentForm extends Component{
  constructor(){
    super();
    SessionStore.on("change",() => {if (this._mounted) this.forceUpdate()});
    this.state = {
      rating:3,
      tekst: ""
    };
  }

  componentWillMount() {

  }


  componentDidMount() {
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }
  onRateChange(nextValue,prevValue,name){
    this.setState({rating:nextValue});
  }

  onTextChange(e){
    this.setState({tekst: e.target.value});
  }

  createComment(){
    if (this.state.tekst.length > 0) {
      CommentActions.createComment(SessionStore.getOrgnr(),SessionStore.getNavn(),SessionStore.getFbid(),this.state.rating,this.state.tekst);
      this.setState({
        rating:3,
        tekst: ""
      })
    }
  }

  getValidationState() {
    const length = this.state.tekst.length;
    if (length > 1) return 'success';
    else if (length >= 0) return 'warning';
  }


  render() {
    var CommentBTN;
    if (SessionStore.isLoggedIn()) {
      CommentBTN = <Button className="CommentButton" bsStyle="warning" onClick={this.createComment.bind(this)}>Kommenter</Button>
    } else {
      CommentBTN = <Button className="CommentButton" disabled bsStyle="warning" onClick={this.createComment.bind(this)}>Du må være logget inn for å kommentere!</Button>
    }
    return (
      <form>
        <hr/>
        <h3>Legg igjen din vurdering her</h3>
        <div className="StarComponent">
          <StarRatingComponent name="commentRating" value={this.state.rating} emptyStarColor="#BBB" onStarClick={this.onRateChange.bind(this)} />
        </div>
        <FormGroup validationState={this.getValidationState()}>
          <FormControl value={this.state.tekst} onChange={this.onTextChange.bind(this)} componentClass="textarea" placeholder="skriv din kommentar.." />

          <HelpBlock>Vennligst begrunn ratingen med en kommentar!</HelpBlock>
          {CommentBTN}
        </FormGroup>
      </form>
    );
  }

}
