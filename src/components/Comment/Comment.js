import React, {Component} from "react"
import StarRatingComponent from "react-star-rating-component";
import "./Comment.css"
import moment from 'moment';

/*
Code to display individual comments. Receives a comment object as a prop
*/

export default class Comment extends Component {
  constructor(props){
    super(props);
    this.state = {
      comment: {
        orgnr: 0,
        navn: "",
        fbid: 0,
        tekst: "",
        rating: 0,
        tid: 0
      },
      timestring: "",
      location: ""
    }
  }

  componentWillMount() {
    this.setState({
      comment: this.props.comment
    },this._updateMomentText);
    this.interval = setInterval(this._updateMomentText.bind(this), 10000);
  }

  componentDidMount() {
    fetch('http://hotell.difi.no/api/json/mattilsynet/smilefjes/tilsyn?=&orgnummer='+this.state.comment.orgnr).then(response => {
      response.json().then(data => {
        console.log(data.entries[0].navn);
        this.setState({location: " @ "+data.entries[0].navn})
      })
    })
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  _updateMomentText(){
    this.setState({ timestring: moment.unix(this.state.comment.tid/1000).fromNow() });
  }


  render() {

    return(
      <div className="Comment">
        <p className="CommentText">{this.state.comment.tekst}</p>
        <hr />
        <div className="CommentFooter">
          <StarRatingComponent name="displayrating" value={this.state.comment.rating} emptyStarColor="#BBB" editing={false} />
          <span className="CommentInfo">{this.state.timestring} by {this.state.comment.navn} {this.state.location}</span>
        </div>
      </div>
    )
  }

}
