import dispatcher from "../dispatcher";
import { EventEmitter } from "events";
import SessionStore from "./SessionStore";
import querystring from "querystring"

class CommentStore extends EventEmitter {
  constructor() {
    super();
    SessionStore.on("change", () => {
      var url = 'http://it2810-03.idi.ntnu.no:8080/api/comments/'+SessionStore.getOrgnr();
      if (SessionStore.getOrgnr() === -1) {
        url = 'http://it2810-03.idi.ntnu.no:8080/api/comments/user/'+SessionStore.getFbid()
      }
      fetch(url)
        .then((response) => {
          response.json()
            .then((data) => {
              this.comments = data;
              this.emit("change");
            })
            .catch((error) => console.log("Failure: ",error))
      });
    });
    this.comments = [];
  }

  createComment(orgnr,fbid,navn,rating,tekst){
      const tid = Date.now();

      var data = {orgnr: orgnr,
        fbid: fbid,
        navn: navn,
        rating: rating,
        tekst: tekst,
        tid: tid
      };
      fetch('http://it2810-03.idi.ntnu.no:8080/api/comments/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        },
        body: querystring.stringify(data)
      }).then((data) => {
        console.log('Request success: ',data)
      }).catch((error) => {
        console.log('Request failure: ',error)
      });

      // is Ok cus async
      this.comments.push({
        orgnr,
        fbid,
        navn,
        rating,
        tekst,
        tid
      });

      this.emit("change");
  }

  getComments() {
    return this.comments;
  }

  handleActions(action){
    switch(action.type){
      case "CREATE_COMMENT": {
        this.createComment(action.orgnr,action.fbid,action.navn,action.rating,action.tekst);
        break;
      }
      default: break;
    }
  }
}

const commentStore = new CommentStore();
dispatcher.register(commentStore.handleActions.bind(commentStore));


export default commentStore;
