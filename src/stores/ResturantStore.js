/*
The ResturantStore collects all the resturants from the Mattilsynet API, together with collecting the rating for each resturant from our database.
Also handles which filter is active at all time.
*/
import { EventEmitter } from "events"

import dispatcher from "../dispatcher"

class ResturantStore extends EventEmitter {
  constructor(){
    super();

    this.handleActions = this.handleActions.bind(this);
    this.setFilter = this.setFilter.bind(this);

    this.filter = {
      good: false,
      ok: false,
      bad: false,
      five: false,
      four: false,
      three: false,
      two: false,
      one: false
    };

    this.resturants = [];

    fetch('http://hotell.difi.no/api/json/mattilsynet/smilefjes/tilsyn?poststed=Trondheim')
      .then(response => response.json())
      .then(restaurants => {
        const parsedData = restaurants.entries.map(r => {
          return {
            id: r.orgnummer,
            sakref: r.sakref,
            date: r.dato,
            name: r.navn,
            cleanRate: r.total_karakter,
            adresse: r.adrlinje1,
            poststed: r.poststed
          }
        });
        this.resturants = this.checkDoubleResturants(parsedData);
        this.setRatings();
        this.emit("done");
        this.emit("change");
      })
      .catch(error => console.error(error));
  }

  // Finds resturants that has several supervisions and chooses the latest supervision.
  checkDoubleResturants(resturants) {
    let tempResturants = [];
    for (let i = 0; i < resturants.length-1; i++) {
      if (resturants[i].id !== resturants[i+1].id) {
        tempResturants.push(resturants[i]);
      } else {
        if (resturants[i].cleanRate > resturants[i+1].cleanRate) {
          tempResturants.push(resturants[i]);
          tempResturants.pop(resturants[i+1]);
        }
      }
    }
    return tempResturants
  }

  // Collects the rating from the DB for each resturant and resturns the average rating.
  // The average rating is displayed in the table with all resturants and is also possible to filter on.
  setRatings(){
    this.resturants.forEach((r) => {
      fetch("http://it2810-03.idi.ntnu.no:8080/api/comments/average/"+r.id)
        .then((result) => {
          result.json().then((data) => {
            r.average = data.average;
            this.emit("change")
          })
        })
    })
  }

  // Changes the state of each filter when a filter is checked.
  setFilter({ value }) {
    // value = 'good'
    this.filter[value] = !this.filter[value];
    this.emit('change');
  }

  // Returns all resturants.
  getAllResturants() {
    return this.resturants;
  }

  // Returns all filters, to find if one or more is checked.
  getAllFilters() {
    return this.filter;
  }

  // Handle each action from the ResturantActions, and calls different methods based on the given action.
  handleActions(action){
    switch(action.type) {
      case "SET_RESTUANTS":
        this.setResturants(action.resturants);
        break;
      case "SET_FILTER":
        this.setFilter(action.value);
        break;
      default: break
    }
  }
}

const resturantStore = new ResturantStore();
dispatcher.register(resturantStore.handleActions.bind(resturantStore));

export default resturantStore
