import { EventEmitter } from "events"

import dispatcher from "../dispatcher"

class SessionStore extends EventEmitter {
  constructor(){
    super();

    this.loggedin = false;
    this.navn = "unknown";
    this.fbid = null;
    this.orgnr = "0";
    this.fbUrl = "null"

  }

  changeOrgnr(orgNr){
    this.orgnr = orgNr;
    this.emit("change")
  }
  login(){
    this.loggedin = true;
    this.emit("change");
  }

  logout(){
    this.loggedin = false;
    this.emit("change");
  }

  changeUser(navn,fbid){
    this.navn = navn;
    this.fbid = fbid
  }
  setFbUrl(fbUrl) {
    this.fbUrl = fbUrl
  }

  getNavn(){
    return this.navn
  }

  getFbid(){
    return this.fbid
  }

  getOrgnr(){
    return this.orgnr
  }

  isLoggedIn() {
    return this.loggedin
  }
  getFbUrl() {
    return this.fbUrl
  }

  handleActions(action){
    switch(action.type){
      case "SET_ORGNR": {
        console.log("change received");
        this.changeOrgnr(action.orgnr);
        break;
      }
      case "SET_USER": {
        this.changeUser(action.navn,action.fbid);
        break;
      }
      default: break
    }
  }
}

const sessionStore = new SessionStore();
dispatcher.register(sessionStore.handleActions.bind(sessionStore));

export default sessionStore
