// src/routes.js
import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import App from './components/App';
import DetailView from './components/DetailView/DetailView';
import HomePage from './components/HomePage/HomePage';
import Profile from "./components/Profile/Profile";

const Routes = (props) => (
  <Router {...props}>
    <Route path="/" component={App}>
      <IndexRoute component={HomePage}/>
      <Route path="detail" component={DetailView}/>
      <Route path="profile" component={Profile}/>
    </Route>
  </Router>
);

export default Routes;
