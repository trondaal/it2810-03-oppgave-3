
/*
This file will contain code for comment actions (adding new comments to database)
Reqires mongodb to be running on localhost
*/


import dispatcher from "../dispatcher";

export function createComment(orgnr,navn,fbid,rating,tekst) {
  dispatcher.dispatch({
    type: "CREATE_COMMENT",
    orgnr,
    navn,
    fbid,
    rating,
    tekst
  });
}

export function deleteComment(id) {
  dispatcher.dispatch({
    type: "DELETE_COMMENT",
    id
  });
}
