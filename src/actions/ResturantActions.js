
/*
This file contains the actions for filtering the resturant lists.
The different functions is called base on the type of the request.
*/


import dispatcher from "../dispatcher";

export function saveAllResturants(resturants) {
  dispatcher.dispatch({
    type: "SET_RESTUANTS",
    resturants
  });
}

export function setFilter(value) {
  dispatcher.dispatch({
    type: "SET_FILTER",
    value
  });
}
