

import dispatcher from "../dispatcher"


export function changeOrgnr(orgnr) {
  dispatcher.dispatch({
    type: "SET_ORGNR",
    orgnr
  })
}
