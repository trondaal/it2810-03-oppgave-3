/**
Denne koden lager et REST-api som håndterer kommunikasjonen med
databasen. Merk at maskinen som kjører dette også må kjøre en instans
av mongodb på port 27017
*/
var bodyParser =  require("body-parser");
var Comment = require('./models/comment');
var express = require("express");
var mongoose = require('mongoose');

var url = 'mongodb://localhost:27017/finspist';
var app = express();

var allowCrossDomain = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);

var port = process.env.PORT || 8080;

var router = express.Router();

router.use((req, res, next) => {
  console.log("something");
  next();
});

router.get('/', (req, res) => {
  res.json({ message: 'hooray! welcome to our api!' });
});


router.route('/comments')
  //Sett inn ny kommnentar
  .post((req, res) => {
    var comment = new Comment();

    comment.orgnr = req.body.orgnr;
    comment.navn = req.body.navn;
    comment.fbid = req.body.fbid;
    comment.rating = req.body.rating;
    comment.tekst = req.body.tekst;
    comment.tid = req.body.tid;

    comment.save((err) => {
      if (err) res.send(err);
      res.json({message: "Comment created!"})
    })

  })
  //Hent alle kommentarer
  .get((req, res) => {
    Comment.find((err, comments) => {
      if (err) res.send(err)
      res.json(comments)
    })
  });
//Hent alle kommentarer som hører til et orgnr
router.route('/comments/:orgnr')
  .get((req, res) => {
    Comment.find({orgnr: req.params.orgnr}).exec((err, comments) => {
      if (err) res.send(err);
      res.json(comments);
    })
  });
//Få gjennomsnittsratingen til et serveringssted
router.route('/comments/average/:orgnr')
  .get((req, res) => {
    Comment.find({orgnr: req.params.orgnr}).exec((err, comments) => {
      if (comments.length > 0){
        res.json({average: comments.map((c) => {return c.rating}).reduce((a, b) => {return a+b})/comments.length})
      } else {
        res.json({average: 0})
      }
    })
  })
//Få alle kommentarene til en spesifik bruker
router.route('/comments/user/:fbid')
  .get((req, res) => {
    Comment.find({fbid: req.params.fbid}).exec((err, comments) => {
      if (err) res.send(err);
      res.json(comments);
    })
  });
//Slett en kommentar (ikke i bruk)
router.route('/comments/:fbid/:tid')
  .delete((req, res) => {
    Comment.find({fbid: req.params.fbid, tid: req.params.tid}).remove().exec(() => {
      res.json({message: "Deleted"});
    });
  });
//Hvor mange kommentarer har en bruker laget?
router.route('/user/comments/total/:fbid')
  .get((req, res) => {
    Comment.find({fbid: req.params.fbid}).exec((err, comments) => {
      if (err) res.send(err);
      res.json({total: comments.length});
    })
  })
//Snittet av vureringer en bruker har gitt
router.route('/user/comments/average/:fbid')
  .get((req, res) => {
    Comment.find({fbid: req.params.fbid}).exec((err, comments) => {
      if (comments.length > 0){
        res.json({average: comments.map((c) => {return c.rating}).reduce((a, b) => {return a+b})/comments.length})
      } else {
        res.json({average: 0})
      }
    })
  })




app.use('/api', router);

app.listen(port);
console.log('Magic happens on port ' + port);


mongoose.connect(url);
