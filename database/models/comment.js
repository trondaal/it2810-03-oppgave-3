var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
  orgnr: String,
  navn: String,
  fbid: String,
  rating: Number,
  tekst: String,
  tid: Number,
})

module.exports = mongoose.model('Comment', CommentSchema);
