# finSpist - Dokumentasjon

### Nettsiden
Nettisden kjører på denne linken: http://it2810-03.idi.ntnu.no

Dette fyller dette kravet:

* Webapplikasjonen skal kjøres på gruppaas virtuelle maskin og bruke node.js på serversiden. Det er selvsagt en fordel å kunne utvikle og teste på egne maskiner.

### Tema for siden
Dette dokumentet inneholder dokumentasjon til finSpist, en side for å se hvordan smilefjes Mattilsynet har gitt resturanter i Trondheim. Sammen med brukerene sine egne kommentarer og vurderinger av resturanter ønsker vi å gi deg en oversikt over de beste og mest renslige resturantene i Trondheim.

Datane er hentet fra dette APIet til Mattilsynet: https://data.norge.no/data/mattilsynet/smilefjestilsyn-på-serveringssteder

### Arkitektur
I vårt prosjekt har vi valgt å bruke data fra mattilsynet og data fra vår egen database som har oversikt over kommentarer og vurderinger av restaurantene. Disse kommentarene er relatert til en resturant og en bruker som logger seg inn ved hjelp av facebook. Databasen er MongoDB All dataen blir prosessert på vår node.js server før det vises på nettsiden.

![alt tag](Arkitektur.png)

### Valg av teknologi
Vi har valgt å bruke React i dette prosjektet. Det er flere grunner til det. For det første synes vi at det var lettere å komme i gang med React. Vi opplevde mye mindre problemer med å få koden til å fungere som vi ønsket.

Det var også mye lettere å finne guider og dokumentasjon på nett (ikke så rart siden React er en del eldre). Da vi jobbet med Angular hadde vi mye mer problemer med å få ganske enkle ting til å fungere.

Omfanget av prosjektet tatt i betraktning mener vi og at det er greit å bygge det opp med de bibliotekene vi vil ha framfor å bruke et stort rammeverk. I forbindelse med dette ønsker vi å bruke Flux akritekturen for å håndtere data flyten og staten på daten vi bruker. Dette gjør at vi ikke trenger å sende dataene fram og tilbake i de ulike komponentene, men ha actions og stores som lagrer dataen.

Flux inneholder fire ulike komponenter:

* Actions: Inneholder hjelpe metoder som passer på å sende dataen på riktig tidspunkt til Dispatcheren.
* Dispatcher: Mottar actions, eller instrukser og kjører disse.
    * Stores: Holder på statene og logikken til dataen, det er disse Dispatcheren kjører.
    * Controller Views: Er alle filene som henter staten fra Stores, de kan oppdatere Storen og hente datene derfra.

Dette fyller dette kravet:

* Webapplikasjonen skal bruke React eller Angular 2.0, men det er greit å i tillegg bruke andre bibliotek eller løsninger som dere finner hensiktsmessig.

### Installasjon og Kjøring
git clone [git@bitbucket.org:trondaal/it2810-03-oppgave-3.git](git@bitbucket.org:trondaal/it2810-03-oppgave-3.git)

1. cd it2810-03-oppgave-3
2. npm install
3. npm start
4. gå til http://localhost:3000 i nettleseren din.

### Oversikt Over Mappestruktur
**public**

Denne mappen inneholder HTML siden som blir manipulert og rendret på nytt med innhold fra de ulike komponentene i src/components/

**server**

I server mappen ligger express serveren vår som kjører prosjektet.

**src**

Src-mappen inneholder koden til nettsiden. Her er det og diverse routingfiler osv.

**src > actions**

Actions-mappen skal inneholde flux-actions

**src > components**

Denne mappen inneholder de individuelle komponentene til siden vår. Hver komponent har sin egen mappe.


**src > stores**
skal inneholde flux-storene for å lagre data til applikasjonen

### API###

Vi opprettet et REST-api til å lagre dataen som programmet bruker. Det finnes her:

[http://it2810-03.idi.ntnu.no:8080/api/](http://it2810-03.idi.ntnu.no:8080/api/)

**For å hente alle kommentarer:**

[http://it2810-03.idi.ntnu.no:8080/api/comments/](http://it2810-03.idi.ntnu.no:8080/api/comments/)



### Komponenter osv.

**Kart**

Denne komponenten oppfyller dette kravet:

* Webapplikasjonen skal ha et litt "fancy" alternativ visning av listen f.eks. visning på kart eller visuell grafisk fremstilling av data, ordsky ea.
    * Vi visualiserer alle resturantene i Trondheim i et kart

Vi skal vise et kart over restauranter i byen. Kartet et et utsnitt over Trondheim hvor hver restaurant er markert som en prikk med en kniv og gaffel. Hvis man trykker på knappen kommer man inn i detail-view over restauranten.

**Resturant komponentene (src/components/ResturantView og src/components/ResturantFilter)**

Disse komponentene oppfyller disse kravene:

* Ta utgangspunkt i en applikasjon av typen "katalog" hvor brukeren skal få presentert en liste av noe ("enheter" som kan være produkter for salg eller leie; bilder, musikk, film i en samling; personer som er medlemmer i noe; eller hva som helst annet som kan presenteres som enkeltenheter i en liste).
    * Resturantene fra mattilsynet vises i en liste.
* Brukergrensensittet skal ha listebasert visning med få detaljer for hver enhet, og hvor målet er å vise brukeren hva som er i databasen eller hva som er resultatet av et søk. Brukeren skal ha mulighet til å se flere detaljer for hver enhet enhet enten i et eget vindu, eller ved at listen enheten i lista har expand/collpase egenskap.
    * Resturantene er visualisert i en liste, der man kan trykke på en resturant og få opp informasjon om resturanten og alle kommentarer og vurdering resturanten har.
* Den listebaserte visningen skal kunne sorteres på minimum to forskjellge egenskaper. Eksempel: etter at brukeren har fått retunert en liste etter et søk skal brukeren kunne bytte mellom forskjellige sorteringer.
    * Sorteres på navn og vurdering.
* Den listebaserte visningen skal kunne filtreres på minimum to forskjellge egenskaper. Eksempel: etter at brukeren har fått returnert en liste etter et søk skal brukeren kunne krysse av på en egenskap for å få begrenset antallet enheter i resultatsettet til kun de som har denne egenskapen.
    * Filtreres på smilefjes og vurdering.

Resturant komponentene viser en oversikt over alle resturantene som finnes i Trondheim. Vi henter resturantene fra Mattilsynets smilefjestilsyn, samtidig får vi informasjon om resultatet fra tilsynset som vi representerer med lignende smiljefjesikon som Mattilsynet gjør. Vi har også laget vår egen vurdering/kommenteringskomponent der gjennomsnittelig vurdering vises i tabellen.

Komponenten inneholder en søkfunksjon som gjør det mulig å søke i alle resturantene fra APIet. Samtidig som vi har laget muligheter for å filtrere på to ulike egenskaper, en for de ulike smilefjesene (denne ser litt rar ut nå, fordi alle resturantene i Trondheim på nåværende tidspunkt har fått karakterene 1 eller 0 som gir stort smilefjes). Den andre egenskapen vi filtrerer på er vurdering, der du kan velge å kun vise de resturantene med 1, 2, 3, 4 eller 5 stjerner. Det siste denne komponenten gjør er å sortere listene, dette kan velges å enten være å sortere på navn, da sorterer man første gang man trykker på kanppen alfabetisk og andre gang alfabetisk med start bakerst, denne toggles og gjør at det blir annenhver gang. Den andre funksjonen det er mulig å sortere på er vurdering, det gjør det samme som på navn, men sorterer den med flest antall stjerner først for så å ha dårligst antall stjerner eller ikke noe vurdering i andre sortering.

*En kjent feil er at om du velger et filter, for eksempel vurdering 3 stjerner og så sorterer på navn eller vurdering vil sorteringen overkjøre filteret og vise alle resturantene igjen. Det fungerer om du sorterer og så filtrerer.*


**Logg inn**

Denne komponenten oppfyller dette kravet:

* Webapplisjonen må implementere "session"-håndtering (som du f.eks. trenger for å implementere dynamisk lasting og min side).

Login komponenten importerer en allerede ferdiglaget komponent for react som tillater enkel facebook innlogging. Denne komponenten håndterer det aller meste av facebook login delen som f.eks hente bruker ID og brukernavn. Facebook ordner automatisk et access token, som definerer hvor lenge brukeren kan være pålogget siden, dette tillater oss å hente informasjon fra brukeren som blir brukt senere til bl.a profilen. Informasjonen som blir hentet fra facebook lagrer vi så i sessionstore slik at vi bl.a kan registrere hvilke brukere som har skrevet hvilke kommentarer. Dette blir gjort ved at vi lagrer den unike facebook ID’en brukeren har og da kan vi lett hente ut hvilke kommentarer en bruker har skrevet.

**Profilside**

For å komme inn på profilsiden trykker man på navnet sitt i navbaren.

Denne komponenten oppfyller dette kravet:

* Webapplikasjonen skal ha "min side" funksjonalitet som i praksis betyr at en bruker skal kunne logge seg på og at det blir registrert noe fra søkeaktiviteten f.eks. hva brukeren har sett på eller søkene som er brukt.

Dette er en simpel profilside som henter kommentarene brukeren har skrevet. For å “spiffe” opp komponenten litt henter vi også antall kommentarer og gjennomsnitlig vurdering i tallformat.

**Detalj visning av en resturant**

Denne siden viser detaljer om en restaurant. Her vises et bilde (automatisk hentet fra Google Street View, så det er litt tilfeldig om det faktisk viser restauranten), adresse, smilefjesrating og gjennomsnitlig rating fra brukerne. Den viser også kommentarene som er lagt igjen av andre brukere. Dersom man er logget inn kan man legge igjen en vurdering og kommentar.

Disse komponenten oppfyler disse kravene:

* I webappliksjonen skal det inngå en database som kjøres på gruppas virtuelle maskin. Type database og hvordan denne brukes er opp til dere å bestemme. Dere skal demonstrere både skriving og lesing til databasen fra webapplikasjonen inklusive en form for søk (i praksis dynamisk brukerdefinert utvalg av det som skal vises). Generelt er det mye artigere å jobbe med en datamengde som gir et realistisk inntrykk (eksempevis mulig å søke på forskjellige ting og få resultatsett som er forskjellige og har forskjellig antall).
    * En MongoDB databse som vi skriver nye kommentarer og vurderinger til, leser eksistrende kommentarer og vurderinger.
* Den listebaserte visningen skal ha dynamisk lasting av data. Eksempel: etter et søk vises de 10 første treffene, men flere lastes når brukeren scroller eller ved blaing i sider.
    * Du kan laste inn flere kommentarer på detajlesiden til hver resturant
